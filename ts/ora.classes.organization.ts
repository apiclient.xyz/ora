import * as plugins from './ora.plugins';
import { Ora } from './ora.classes.ora';
import { OraProject } from './ora.classes.project';

export interface IOraOrganization {
  created_at: string;
  description: string;
  id: number;
  member_type: number;
  name: string;
  org_picture: string;
  org_type: number;
  owner_id: number;
  premium_plan: null;
  quantity: number;
  updated_at: string;
  web: string;
}

export class OraOrganization implements IOraOrganization {
  // ======
  // STATIC
  // ======
  public static async getAllOrganizations(oraRef: Ora): Promise<OraOrganization[]> {
    const response = await oraRef.request('/organizations', 'GET');
    const organizations: OraOrganization[] = [];
    for (const orgData of response.data) {
      organizations.push(new OraOrganization(oraRef, orgData));
    }
    return organizations;
  }

  public created_at: string;
  public description: string;
  public id: number;
  public member_type: number;
  public name: string;
  public org_picture: string;
  public org_type: number;
  public owner_id: number;
  public premium_plan: null;
  public quantity: number;
  public updated_at: string;
  public web: string;

  // ========
  // INSTANCE
  // ========
  public oraRef: Ora;

  constructor(oraRefArg: Ora, creationObjectArg: IOraOrganization) {
    this.oraRef = oraRefArg;
    Object.assign(this, creationObjectArg);
  }

  public async getProjects(): Promise<OraProject[]> {
    return OraProject.getAllProjectsForOrganization(this);
  }
}
