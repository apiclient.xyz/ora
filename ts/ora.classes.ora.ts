import * as plugins from './ora.plugins';
import { OraOrganization } from './ora.classes.organization';

export class Ora {
  public apiBase: string = 'https://api.ora.pm';
  private apiToken: string;

  constructor(apiTokenArg: string) {
    this.apiToken = apiTokenArg;
  }

  public async getOrganizations() {
    return await OraOrganization.getAllOrganizations(this);
  }

  /**
   * make a request
   * @param routeArg
   * @param methodArg
   * @param payloadArg
   */
  public async request(routeArg: string, methodArg: string, payloadArg?: string) {
    const response = await plugins.smartrequest.request(this.apiBase + routeArg, {
      method: methodArg,
      requestBody: payloadArg,
      headers: {
        accept: 'application/json',
        authorization: `Bearer ${this.apiToken}`
      }
    });
    return response.body;
  }
}
